# bootstrap a linux VM to setup a  test client for our nas system

## what you get

- scripted setup using an ansible playbook, works for RHEL 7 and 8
- no need for central ansible host, just run the playbook on the target machine
- no ansible configuration required.
- automatic daily yum/dnf updates by yum-cron or dnf-automatic
- install vmware tools
- configure /home on /dev/sdb
- add 4ea accounts
- add public keys to authorized_keys. (for 4ea accounts)
- add a startup check _no-login-without-snapshot.service_. See  if we run on a VM snapshot or the master. On master allow root login only, no 4ea accounts. To reduce the risk of accidental changes on master.
- give full sudo access to all 4ea accounts using group wheel
- no password login for root on ssh. password login on vm-console works
- set root password
- add public key for root accounts
- set LANG and LC_CTYPE in /etc/environment

## what's missing

- we still have a swap partiton on /dev/sda - move it to sdb, i.e. don't include it in snapshots
- one or some playbooks for nfs client setup, smb client setup, to easily restore standard configurations
- working with snapshost doesn't feel very smooth.

## requirements

- /dev/sda - system
- /dev/sdb - empty 2nd disk - independent, not in snapshots
- network access to gitlab.ethz.ch
- ansible installed
- root login


## download ansible playbook

login as root
```terminal
# cd /root
#  git clone https://gitlab.ethz.ch/scaleadmin/str-client-setup.git
```
 or
```terminal
# cd /root
# git clone git@gitlab.ethz.ch:scaleadmin/str-client-setup.git

Cloning into 'str-client-setup'...
remote: Enumerating objects: 29, done.
remote: Counting objects: 100% (29/29), done.
remote: Compressing objects: 100% (16/16), done.
remote: Total 29 (delta 7), reused 29 (delta 7), pack-reused 0
Unpacking objects: 100% (29/29), 4.28 KiB | 438.00 KiB/s, done.
```

## get ansible.posix

```terminal
#  https_proxy='https://proxy.ethz.ch:3128' ansible-galaxy collection install ansible.posix
Process install dependency map
Starting collection install process
Installing 'ansible.posix:1.2.0' to '/root/.ansible/collections/ansible_collections/ansible/posix'
```

## run playbook

```terminal
# cd /root/str-client-setup/setup/
# ./run-setup.sh
```

or just

```terminal
# cd /root/str-client-setup/setup/
# ansible-playbook -c localhost ./setup.yml
```

please ignore
```
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'
```

example output
```
PLAY [setup VM as nas test client] ************************************************************************************************************************

TASK [Gathering Facts] ************************************************************************************************************************************
ok: [localhost]

TASK [install smartmontool] *******************************************************************************************************************************
ok: [localhost]

TASK [check if /dev/sda is thin provisioned, i.e. a snapshot disk] ****************************************************************************************
ok: [localhost]

TASK [fail if /dev/sda/ is not thin provisioned, i.e. no snapshot] ****************************************************************************************
skipping: [localhost]

TASK [install vmware tools] *******************************************************************************************************************************
ok: [localhost]

TASK [enable vmtoolsd service] ****************************************************************************************************************************
ok: [localhost]

TASK [install yum-cron RHEL7] *****************************************************************************************************************************
ok: [localhost]

TASK [write /etc/yum/yum-cron.conf RHEL7] *****************************************************************************************************************
ok: [localhost]

TASK [enable and start yum-cron RHEL7] ********************************************************************************************************************
ok: [localhost]

TASK [install dnf-automatic RHEL8] ************************************************************************************************************************
skipping: [localhost]

TASK [write /etc/dnf/automatic.conf RHEL8] ****************************************************************************************************************
skipping: [localhost]

TASK [enable and start dnf-automatic.timer RHEL8] *********************************************************************************************************
skipping: [localhost]

TASK [vg on /dev/sdb] *************************************************************************************************************************************
ok: [localhost]

TASK [create lv home on vg data (/dev/sdb)] ***************************************************************************************************************
ok: [localhost]

TASK [home as ext4] ***************************************************************************************************************************************
ok: [localhost]

TASK [mount home on /home] ********************************************************************************************************************************
ok: [localhost]

TASK [add accounts] ***************************************************************************************************************************************
ok: [localhost] => (item={u'gecos': u'heinrich billich', u'name': u'hbi4ea', u'uid': 510975})
ok: [localhost] => (item={u'gecos': u'urs gubler', u'name': u'ugu4ea', u'uid': 178856})
ok: [localhost] => (item={u'gecos': u'alexander zimmermann', u'name': u'azi4ea', u'uid': 224768})
ok: [localhost] => (item={u'gecos': u'willi engeli', u'name': u'wen4ea', u'uid': 216627})
ok: [localhost] => (item={u'gecos': u'thomas schlert', u'name': u'schlert', u'uid': 42117})

TASK [add public keys to 4ea accounts] ********************************************************************************************************************
ok: [localhost] => (item=hbi4ea)
ok: [localhost] => (item=ugu4ea)
ok: [localhost] => (item=wen4ea)

TASK [give group wheel sudo access without password] ******************************************************************************************************
ok: [localhost]

TASK [no-login-without-snapshot.sh] ***********************************************************************************************************************
ok: [localhost]

TASK [no-login-without-snapshot.service] ******************************************************************************************************************
ok: [localhost]

TASK [enable no-login-without-snapshot service] ***********************************************************************************************************
ok: [localhost]

TASK [add public keys for root] ***************************************************************************************************************************
ok: [localhost]

TASK [sshd permitrootlogin prohibit-password] *************************************************************************************************************
ok: [localhost]

TASK [set root password] **********************************************************************************************************************************
ok: [localhost]

PLAY RECAP ************************************************************************************************************************************************
localhost                  : ok=21   changed=0    unreachable=0    failed=0    skipped=4    rescued=0    ignored=0   
```

