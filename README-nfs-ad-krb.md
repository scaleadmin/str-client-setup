# join AD

WORK IN PROGRESS
```
# OU="OU=spectrumscale,OU=computers,OU=NAS-Service,OU=Storage,OU=ID-SD,OU=Hosting,DC=d,DC=ethz,DC=ch"

# realm join d.ethz.ch \
   --user=hbi4ea \
   --computer-ou=$OU \
   --automatic-id-mapping=no

```

restrict logins
```
realm deny --all
realm permit billichh@D.ETHZ.CH
realm permit -g 'id-sd-storage-nasadmin@D.ETHZ.CH'
```

```
# realm discover d.ethz.ch
d.ethz.ch
  type: kerberos
  realm-name: D.ETHZ.CH
  domain-name: d.ethz.ch
  configured: kerberos-member
  server-software: active-directory
  client-software: sssd
  required-package: oddjob
  required-package: oddjob-mkhomedir
  required-package: sssd
  required-package: adcli
  required-package: samba-common-tools
  login-formats: %U@d.ethz.ch
  login-policy: allow-realm-logins
[root@str-client-01 ~]#
```

the first lookup in AD may take some minutes

```
# getent passwd billichh@d.ethz.ch
billichh@d.ethz.ch:*:227005:1029:Heinrich Rainer Billich:/nas/billichh:/bin/bash
```

```
# id billichh@d.ethz.ch
uid=227005(billichh@d.ethz.ch) gid=1029(domain users@d.ethz.ch) groups=1029(domain users@d.ethz.ch),76558(id-sd-nas-group_custom@d.ethz.ch),434518(id-sd-storage-nas-team@d.ethz.ch),201(id-sd-storage-nasadmin@d.ethz.ch),447761(id-clientbackup-500gb@d.ethz.ch),436178(mitarbeitende_21_86@d.ethz.ch),...
```

```
# getent group id-sd-storage-nas-team@d.ethz.ch
id-sd-storage-nas-team@d.ethz.ch:*:434518:billichh@d.ethz.ch,gubleru@d.ethz.ch,hbi4ea@d.ethz.ch,azi4ea@d.ethz.ch,ste4ea@d.ethz.ch,ugu4ea@d.ethz.ch,steiger@d.ethz.ch,awengeli@d.ethz.ch,wengeli@d.ethz.ch,schlert@d.ethz.ch
```

/etc/krb5.conf - ticket lifetime 1h
```
[libdefaults]
 dns_lookup_realm = false
 ticket_lifetime = 1h
 renew_lifetime = 7d
 forwardable = true
 rdns = false
 pkinit_anchors = /etc/pki/tls/certs/ca-bundle.crt
 default_ccache_name = KEYRING:persistent:%{uid}
 default_realm = D.ETHZ.CH

```
```
```
/etc/sssd/sssd.conf
```
# use_fully_qualified_names = True
use_fully_qualified_names = False
```
```
[root@id-sto-clnt01 sssd]# id billichh
uid=168910149(billichh) gid=1987600513(domain users) ...
```


```
[hbi4ea@str-client-01 ~]$ kinit
Password for hbi4ea@D.ETHZ.CH:
[hbi4ea@str-client-01 ~]$ klist
Ticket cache: KEYRING:persistent:510975:510975
Default principal: hbi4ea@D.ETHZ.CH

Valid starting       Expires              Service principal
04/14/2021 14:24:47  04/15/2021 00:24:47  krbtgt/D.ETHZ.CH@D.ETHZ.CH
	renew until 04/21/2021 14:24:39
```